local lujavrite = require "lujavrite"

-- Determine Java home to use
java_home = os.getenv("JAVA_HOME")
if java_home == nil then
   java_home = "/usr/lib/jvm/jre"
end

-- Initialize JVM
lujavrite.init(java_home .. "/lib/server/libjvm.so", "-ea", "-esa")

-- System.getProperty(key)
function get_property(key)
   return lujavrite.call(
      "java/lang/System", "getProperty",
      "(Ljava/lang/String;)Ljava/lang/String;",
      key
   )
end

-- System.setProperty(key, value)
function set_property(key, value)
   return lujavrite.call(
      "java/lang/System", "setProperty",
      "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;",
      key, value
   )
end

local java_version = get_property("java.version")
print("Java version is " .. java_version)

set_property("foo", "bar")
print("foo is " .. get_property("foo"))

local java_nil = lujavrite.call("java/lang/String", "valueOf", "(Ljava/lang/Object;)Ljava/lang/String;", nil)
print("nil in Lua is " .. java_nil .. " in Java")
